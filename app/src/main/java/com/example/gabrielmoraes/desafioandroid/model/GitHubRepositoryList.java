package com.example.gabrielmoraes.desafioandroid.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by gabri on 22/11/2016.
 */
public class GitHubRepositoryList implements Serializable {

    public List<Repository> items;
}
