package com.example.gabrielmoraes.desafioandroid.model;

import java.io.Serializable;

/**
 * Created by gabri on 22/11/2016.
 */
public class Repository implements Serializable{

    public String name;
    public Owner owner;
    public String description;
    public int forks_count;
    public int stargazers_count;
}
