package com.example.gabrielmoraes.desafioandroid.model;

import java.io.Serializable;

/**
 * Created by gabri on 23/11/2016.
 */
public class PullRequest implements Serializable{
    public String title;
    public String html_url;
    public String body;
    public String created_at;
    public PullRequestOwner user;

}
