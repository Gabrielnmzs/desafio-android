package com.example.gabrielmoraes.desafioandroid.connections;

import java.util.List;

import com.example.gabrielmoraes.desafioandroid.model.PullRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by gabri on 23/11/2016.
 */
public interface PullRequestRepositoryService {

    public static final String BASE_URL = "https://api.github.com/repos/";

    @GET("{creator}/{repository}/pulls")
    Call<List<PullRequest>> listPullRequest(@Path("creator") String creator, @Path("repository") String repository);
}
