package com.example.gabrielmoraes.desafioandroid.connections;

import com.example.gabrielmoraes.desafioandroid.model.GitHubRepositoryList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by gabri on 22/11/2016.
 */
public interface GitHubRepositoryService {

    public static final String BASE_URL = "https://api.github.com/";


    @GET("search/repositories?q=language:Java&sort=stars")
    Call<GitHubRepositoryList> listRepositories(@Query("page") int page);
}
