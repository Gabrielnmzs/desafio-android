package com.example.gabrielmoraes.desafioandroid.model;

import java.io.Serializable;

/**
 * Created by gabri on 23/11/2016.
 */
public class PullRequestOwner implements Serializable{

    public String login;
    public String avatar_url;
}
