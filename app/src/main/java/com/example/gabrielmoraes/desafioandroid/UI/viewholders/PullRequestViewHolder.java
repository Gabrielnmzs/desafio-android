package com.example.gabrielmoraes.desafioandroid.UI.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.gabrielmoraes.desafioandroid.R;
import com.example.gabrielmoraes.desafioandroid.UI.listeners.RecyclerViewOnClickListener;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by gabri on 23/11/2016.
 */
public class PullRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView pullRequestUserTextView;
    public TextView pullRequestBodyTextView;
    public TextView pullRequestUserNameTextView;
    public CircleImageView pullRequestUserImageView;
    public TextView dataTextView;
    public RecyclerViewOnClickListener listener;

    public PullRequestViewHolder(View itemView) {
        super(itemView);

        pullRequestUserTextView = (TextView) itemView.findViewById(R.id.pullRequestUserTextView);
        pullRequestBodyTextView = (TextView) itemView.findViewById(R.id.pullRequestBodyTextView);
        pullRequestUserImageView = (CircleImageView) itemView.findViewById(R.id.pullrequestUserImage);
        pullRequestUserNameTextView = (TextView) itemView.findViewById(R.id.pullRequestUserName);
        dataTextView = (TextView) itemView.findViewById(R.id.dataTextView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(listener!=null){
            this.listener.onClickListener(view,getAdapterPosition());
        }
    }

    public void setListener(RecyclerViewOnClickListener l){
        this.listener = l;
    }
}
