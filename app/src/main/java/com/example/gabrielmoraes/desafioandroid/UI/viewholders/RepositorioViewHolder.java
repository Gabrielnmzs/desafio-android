package com.example.gabrielmoraes.desafioandroid.UI.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.gabrielmoraes.desafioandroid.R;
import com.example.gabrielmoraes.desafioandroid.UI.listeners.RecyclerViewOnClickListener;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by gabri on 21/11/2016.
 */
public class RepositorioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



    public TextView nomeRepo;
    public TextView descricaoRepo;
    public TextView ownerUserName;
    public TextView starsCount;
    public TextView forksCount;
    public CircleImageView ownerImageView;
    public RecyclerViewOnClickListener listener;

    public RepositorioViewHolder(View itemView){
        super(itemView);

        nomeRepo = (TextView) itemView.findViewById(R.id.repoNameTextView);
        descricaoRepo = (TextView) itemView.findViewById(R.id.repoDescriptionTextView);
        starsCount = (TextView) itemView.findViewById(R.id.starsCountTextView);
        forksCount = (TextView) itemView.findViewById(R.id.forksCountTextView);
        ownerImageView = (CircleImageView) itemView.findViewById(R.id.profile_image);
        ownerUserName = (TextView) itemView.findViewById(R.id.ownerUserNameTextView);
        itemView.setOnClickListener(this);
    }

    public void setListener(RecyclerViewOnClickListener l){
        this.listener = l;
    }

    @Override
    public void onClick(View view) {

        //check if object is null cause in adapter, setListener is not an obligation
        if(listener!=null){
            this.listener.onClickListener(view,getAdapterPosition());
        }

    }
}
