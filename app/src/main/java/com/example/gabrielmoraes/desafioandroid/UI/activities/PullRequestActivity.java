package com.example.gabrielmoraes.desafioandroid.UI.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.gabrielmoraes.desafioandroid.R;
import com.example.gabrielmoraes.desafioandroid.UI.listeners.RecyclerViewOnClickListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import com.example.gabrielmoraes.desafioandroid.events.MessageEvent;
import com.example.gabrielmoraes.desafioandroid.events.RepositoryClickEvent;
import com.example.gabrielmoraes.desafioandroid.UI.adapters.PullRequestAdapter;
import com.example.gabrielmoraes.desafioandroid.connections.PullRequestRepositoryService;
import com.example.gabrielmoraes.desafioandroid.model.PullRequest;
import com.example.gabrielmoraes.desafioandroid.model.Repository;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class PullRequestActivity extends AppCompatActivity implements RecyclerViewOnClickListener {


    private Retrofit retrofit;
    private Toolbar mToolBar;
    private Repository repository;
    private ArrayList<PullRequest> pullRequestList;
    private SuperRecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private PullRequestAdapter adapter;
    private static final String CURRENT_OBJECT = "current_object";
    private SwipeRefreshLayout swipeContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        EventBus.getDefault().register(this);



        toolBarConfiguration();
        retrofitConfiguration();

        pullRequestList = new ArrayList<>();


        mRecyclerView = (SuperRecyclerView)findViewById(R.id.rv_pullRequestList);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainerPullRequest);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.

                repositoryRequest();
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        if (savedInstanceState!=null){
            this.pullRequestList = (ArrayList<PullRequest>) savedInstanceState.getSerializable(CURRENT_OBJECT);
            this.recyclerViewConfiguration();
        }
        else{
            repositoryRequest();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(CURRENT_OBJECT,this.pullRequestList);

    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void receiveObjectFromActivity(RepositoryClickEvent event) {
        this.repository = event.repo;
    }


    public void toolBarConfiguration(){
        this.mToolBar = (Toolbar)findViewById(R.id.tb_pullRequest);
        this.mToolBar.setTitle(this.repository.name);
        setSupportActionBar(this.mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void retrofitConfiguration(){

        this.retrofit = new Retrofit.Builder()
                .baseUrl(PullRequestRepositoryService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }



    public void repositoryRequest(){


        PullRequestRepositoryService service = this.retrofit.create(PullRequestRepositoryService.class);
        Call<List<PullRequest>> requestRepositoryList = service.listPullRequest(this.repository.owner.login,this.repository.name);

        requestRepositoryList.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if (!response.isSuccessful()){


                    if (swipeContainer.isRefreshing()){
                        swipeContainer.setRefreshing(false);
                    }

                }
                else{

                    pullRequestList = (ArrayList<PullRequest>) response.body();
                    if (swipeContainer.isRefreshing()){
                        swipeContainer.setRefreshing(false);
                    }
                    mRecyclerView.hideProgress();
                    recyclerViewConfiguration();
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                String erroMessage = getResources().getString(R.string.erro_conexao);
                EventBus.getDefault().post(new MessageEvent(erroMessage));
                if (swipeContainer.isRefreshing()){
                    swipeContainer.setRefreshing(false);
                }
                mRecyclerView.hideProgress();
            }
        });
    }

    public void recyclerViewConfiguration(){

        if (this.adapter == null){

            this.mLayoutManager = new LinearLayoutManager(this);
            this.mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mRecyclerView.setLayoutManager(this.mLayoutManager);

            adapter = new PullRequestAdapter(this,this.pullRequestList);
            adapter.setOnClickListener(this);
            mRecyclerView.setAdapter(adapter);

        }
        else{
            adapter.notifyDataSetChanged();

        }


    }



    @Override
    public void onClickListener(View v, int position) {

        String html_url = this.pullRequestList.get(position).html_url;


        Intent openPullRequestInBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(html_url));
        startActivity(openPullRequestInBrowser);

    }
}
