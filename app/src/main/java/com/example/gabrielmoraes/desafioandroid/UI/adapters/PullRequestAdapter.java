package com.example.gabrielmoraes.desafioandroid.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.gabrielmoraes.desafioandroid.R;
import com.example.gabrielmoraes.desafioandroid.UI.listeners.RecyclerViewOnClickListener;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.example.gabrielmoraes.desafioandroid.model.PullRequest;
import com.example.gabrielmoraes.desafioandroid.UI.viewholders.PullRequestViewHolder;

/**
 * Created by gabri on 23/11/2016.
 */
public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestViewHolder> {

    private ArrayList<PullRequest> pullRequestList;
    private LayoutInflater mLayoutInflater;
    private Context context;
    private RecyclerViewOnClickListener listener;


    public PullRequestAdapter(Context c, ArrayList<PullRequest> list){

        this.pullRequestList = list;
        this.context = c;
        this.mLayoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.pull_request_item, parent, false);

        PullRequestViewHolder pVH = new PullRequestViewHolder(v);
        pVH.setListener(this.listener);

        return pVH;
    }

    @Override
    public void onBindViewHolder(PullRequestViewHolder holder, int position) {

            holder.pullRequestUserTextView.setText(this.pullRequestList.get(position).title);
            holder.pullRequestBodyTextView.setText(this.pullRequestList.get(position).body);
            holder.pullRequestUserNameTextView.setText(this.pullRequestList.get(position).user.login);
            holder.dataTextView.setText(dateFormat(this.pullRequestList.get(position).created_at));
            Picasso.with(this.context).load(this.pullRequestList.get(position).user.avatar_url).into(holder.pullRequestUserImageView);

    }

    @Override
    public int getItemCount() {
        return pullRequestList.size();
    }

    public void setOnClickListener(RecyclerViewOnClickListener l){
        this.listener = l;

    }

    public String dateFormat(String data){

        SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.Z");
        Date date = new Date();
        try {
             date = dateFormater.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return  new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(date);


    }
}
