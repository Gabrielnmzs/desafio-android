package com.example.gabrielmoraes.desafioandroid.UI.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.gabrielmoraes.desafioandroid.R;
import com.example.gabrielmoraes.desafioandroid.UI.listeners.RecyclerViewOnClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.example.gabrielmoraes.desafioandroid.model.Repository;
import com.example.gabrielmoraes.desafioandroid.UI.viewholders.RepositorioViewHolder;

/**
 * Created by gabri on 21/11/2016.
 */
public class RepositoriosAdapter extends RecyclerView.Adapter<RepositorioViewHolder> {

    private List<Repository> repoList;
    private LayoutInflater mLayoutInflater;
    private Context context;
    private RecyclerViewOnClickListener listener;

    public RepositoriosAdapter(Context c, List<Repository> l){

        this.repoList = l;
        this.context = c;
        this.mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RepositorioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = mLayoutInflater.inflate(R.layout.repositorio_item, parent, false);

        RepositorioViewHolder mVH = new RepositorioViewHolder(v);
        mVH.setListener(this.listener);

        return mVH;
    }

    @Override
    public void onBindViewHolder(RepositorioViewHolder holder, int position) {

        Repository repository = this.repoList.get(position);

        holder.nomeRepo.setText(repository.name);
        holder.forksCount.setText(Integer.toString(repository.forks_count));
        holder.starsCount.setText(Integer.toString(repository.stargazers_count));
        holder.descricaoRepo.setText(repository.description);
        holder.ownerUserName.setText(repository.owner.login);
        Picasso.with(this.context).load(repository.owner.avatar_url).into(holder.ownerImageView);


    }

    @Override
    public int getItemCount() {
        return this.repoList.size();
    }

    public void addAll(List<Repository> listaRepositorios) {
        int startIndex = repoList.size();
        repoList.addAll(startIndex, listaRepositorios);
        notifyItemRangeInserted(startIndex, listaRepositorios.size());
    }

    public void setOnClickListener(RecyclerViewOnClickListener l){
        this.listener = l;

    }
}
