package com.example.gabrielmoraes.desafioandroid.UI.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.gabrielmoraes.desafioandroid.R;
import com.example.gabrielmoraes.desafioandroid.UI.listeners.RecyclerViewOnClickListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import com.example.gabrielmoraes.desafioandroid.events.MessageEvent;
import com.example.gabrielmoraes.desafioandroid.events.RepositoryClickEvent;
import com.example.gabrielmoraes.desafioandroid.UI.adapters.RepositoriosAdapter;
import com.example.gabrielmoraes.desafioandroid.connections.GitHubRepositoryService;
import com.example.gabrielmoraes.desafioandroid.model.GitHubRepositoryList;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements RecyclerViewOnClickListener, OnMoreListener {

    private Toolbar mToolBar;
    private SuperRecyclerView mRecyclerView;
    private Retrofit retrofit;
    private static final String CURRENT_PAGE = "current_page";
    private static final String CURRENT_OBJECT = "current_object";
    private static final int FIRST_PAGE = 1;
    private int current_page = FIRST_PAGE;
    private GitHubRepositoryList repositoryList;
    private RepositoriosAdapter adapter;
    private LinearLayoutManager llm;
    private SwipeRefreshLayout swipeContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        this.mToolBar = (Toolbar) findViewById(R.id.tb_main);
        this.mToolBar.setTitle(getResources().getString(R.string.main_activity_title));
        this.mToolBar.setLogo(R.drawable.ic_github_circle_white_36dp);
        setSupportActionBar(this.mToolBar);

        mRecyclerView = (SuperRecyclerView )findViewById(R.id.rv_list);
//        mRecyclerView.setHasFixedSize(true);

        this.repositoryList = new GitHubRepositoryList();
        this.repositoryList.items = new ArrayList<>();

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                repositoryRequest(current_page);
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        this.retrofitConfiguration();

        if(savedInstanceState!=null){

            this.repositoryList =(GitHubRepositoryList)savedInstanceState.getSerializable(CURRENT_OBJECT);
            this.current_page = savedInstanceState.getInt(CURRENT_PAGE);
            this.recyclerViewConfiguration();
        }else{
            this.repositoryRequest(current_page);
        }

        mRecyclerView.setupMoreListener(this, 1);


    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(CURRENT_OBJECT,this.repositoryList);
        outState.putInt(CURRENT_PAGE,this.current_page);
    }

    public void retrofitConfiguration(){

        this.retrofit = new Retrofit.Builder()
                .baseUrl(GitHubRepositoryService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public void repositoryRequest(int pageNumber){

        GitHubRepositoryService service = this.retrofit.create(GitHubRepositoryService.class);
        Call<GitHubRepositoryList> requestRepositoryList = service.listRepositories(pageNumber);

        requestRepositoryList.enqueue(new Callback<GitHubRepositoryList>() {
            @Override
            public void onResponse(Call<GitHubRepositoryList> call, Response<GitHubRepositoryList> response) {
                if (!response.isSuccessful()){
                    String message = response.message();
                    if (swipeContainer.isRefreshing()){
                        swipeContainer.setRefreshing(false);
                    }
                    mRecyclerView.hideMoreProgress();
                }
                else{

                    recyclerViewConfiguration();

                    if (swipeContainer.isRefreshing()){
                        swipeContainer.setRefreshing(false);
                    }
                    mRecyclerView.hideMoreProgress();
                    GitHubRepositoryList list = response.body();
                    ((RepositoriosAdapter)mRecyclerView.getAdapter()).addAll(list.items);

                    current_page++;
                }
            }

            @Override
            public void onFailure(Call<GitHubRepositoryList> call, Throwable t) {

                String erroMessage = getResources().getString(R.string.erro_conexao);
                EventBus.getDefault().post(new MessageEvent(erroMessage));
                if (swipeContainer.isRefreshing()){
                    swipeContainer.setRefreshing(false);
                }

            }
        });
    }


    public void recyclerViewConfiguration(){

        if (this.adapter == null){

            this.llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            mRecyclerView.setLayoutManager(llm);

            adapter = new RepositoriosAdapter(this,repositoryList.items);
            adapter.setOnClickListener(this);
            mRecyclerView.setAdapter(adapter);


        }


    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Toast.makeText(this, event.message, Toast.LENGTH_LONG).show();
    }



    @Override
    public void onClickListener(View v, int position) {

        startActivity(new Intent(this, PullRequestActivity.class));

        EventBus.getDefault().postSticky(new RepositoryClickEvent(repositoryList.items.get(position)));

    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        repositoryRequest(current_page);
    }
}
