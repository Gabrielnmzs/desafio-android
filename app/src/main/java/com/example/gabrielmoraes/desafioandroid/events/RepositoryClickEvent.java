package com.example.gabrielmoraes.desafioandroid.events;

import com.example.gabrielmoraes.desafioandroid.model.Repository;

/**
 * Created by gabri on 23/11/2016.
 */
public class RepositoryClickEvent {

    public Repository repo;

    public RepositoryClickEvent(Repository r){
        this.repo = r;
    }
}
