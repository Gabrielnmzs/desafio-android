package com.example.gabrielmoraes.desafioandroid.UI.listeners;

import android.view.View;

/**
 * Created by gabri on 23/11/2016.
 */
public interface RecyclerViewOnClickListener {

    public void onClickListener(View v, int position);
}
